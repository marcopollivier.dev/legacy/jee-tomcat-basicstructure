#Projeto modelo para sistemas JEE7 com Tomcat

## JSF setup

If you are in ServletContainer (like Tomcat or Jetty) alter your pom.xml in order to change the scope of **jsf-api** from
**provided** to **compile**.

## JPA setup

You need to change your **persistence.xml** file and put your login, password and url information about the database.

## Versions
- JEE 7.0
  - CDI 1.2 with Weld 2.2.6
  - JSF 2.2
- Tomcat: v8.0.x

---

## TODO
- Implement a Memory DB (DB2...)
- Implement a embedded Tomcat
- Improve the pom file (Versions on properties...)
